define(["require", "exports", "Phaser", "./Play", "./Boot", "./MyGame"], function (require, exports, Phaser, Play_1, Boot_1, MyGame_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function main() {
        console.log(`Phaser Version: ${Phaser.VERSION}`);
        const config = {
            title: "NINJA... NIN-NIN",
            type: Phaser.AUTO,
            backgroundColor: '#000000',
            parent: 'phaser-example',
            width: 800,
            height: 600,
            physics: {
                default: 'matter',
                matter: {
                    gravity: {
                        y: 1
                    },
                    enableSleeping: false,
                    debug: true
                }
            },
            scene: [Boot_1.default, Play_1.default]
        };
        const myGame = new MyGame_1.default(config);
    }
    // Run debug build (AMD module system, RequireJS)
    main();
});
// Run release build (bundle.min.js, Browserify, UglifyJS)
// window.onload = () => main();
//# sourceMappingURL=main.js.map