import * as Phaser from "Phaser";
import Play from "./Play";
import Boot from "./Boot";
import MyGame from "./MyGame";

function main()
{
    console.log(`Phaser Version: ${Phaser.VERSION}`);

    const config: Phaser.Types.Core.GameConfig = {
        title: "NINJA... NIN-NIN",
        type: Phaser.AUTO,
        backgroundColor: '#000000',
        parent: 'phaser-example',
        width: 800,
        height: 600,
        physics: {
            default: 'matter', 
            matter: {
                gravity: {
                    y: 1
                },
                enableSleeping: false,
                debug: true
            }       
        },  
        scene: [Boot, Play]  
    }
    
    const myGame = new MyGame(config);
}

// Run debug build (AMD module system, RequireJS)
main();

// Run release build (bundle.min.js, Browserify, UglifyJS)
// window.onload = () => main();