import * as Phaser from "Phaser";

export default class Play extends Phaser.Scene {

    platform : Phaser.Physics.Matter.Image;

    constructor() {
        super("Play");
    }



    player : Phaser.Physics.Matter.Sprite;
    stars : Phaser.Physics.Matter.Image[] = [];
    bombs : Phaser.Physics.Matter.Image[] = [];
    platforms : Phaser.Physics.Matter.Image[] = [];
    cursors : Phaser.Types.Input.Keyboard.CursorKeys;
    score : number = 0;
    gameOver : boolean = false;
    scoreText : Phaser.GameObjects.Text;

    playerSpeed : number = 4;
    playerTrust : number = 0.1;

    chained : boolean = false;

    chain : Phaser.Physics.Matter.Image[] = [];
    joints : Phaser.Physics.Matter.PointerConstraint[] = [];
    throwDirection : number = 1;
    throwStrength : number = 0.5;
    minChainLength : number = 50;
    maxChainLength : number = 250;
    ropingSpeed : number = 20;
    maxSegmentLength : number = 22;

    KeyW : Phaser.Input.Keyboard.Key;
    KeyA : Phaser.Input.Keyboard.Key;
    KeyS : Phaser.Input.Keyboard.Key;
    KeyD : Phaser.Input.Keyboard.Key;
    KeySpace : Phaser.Input.Keyboard.Key;
    KeyCtrl : Phaser.Input.Keyboard.Key;


    create()
    {
        this.matter.world.setBounds();

        //  A simple background for our game
        this.add.image(400, 300, 'sky');
    
        //  The platforms group contains the ground and the 2 ledges we can jump on
    
        //  Here we create the ground.
        //  Scale it to fit the width of the game (the original sprite is 400x32 in size)
        this.platforms.push(this.matter.add.image(400, 568, 'ground').setScale(2));
    
        //  Now let's create some ledges
        this.platforms.push(this.matter.add.image(600, 400, 'ground'));
        this.platforms.push(this.matter.add.image(50, 250, 'ground'));
        this.platforms.push(this.matter.add.image(750, 220, 'ground'));

        for (let i = 0; i < this.platforms.length; i++) {
            this.platforms[i].setStatic(true);
        }
    
        // The player and its settings
        this.player = this.matter.add.sprite(100, 450, 'dude');
        this.player.setFixedRotation();
        
    
        //  Player physics properties. Give the little guy a slight bounce.
        this.player.setBounce(0.2);
        this.player.setFriction(0.1,0,1);
        this.player.setCollisionCategory(0b0100);
        this.player.setCollidesWith(0b0001);
        this.player.setMass(1);
    
        //  Our player animations, turning, walking left and walking right.
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });
    
        this.anims.create({
            key: 'turn',
            frames: [ { key: 'dude', frame: 4 } ],
            frameRate: 20
        });
    
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1
        });
    
        //  Input Events
        this.cursors = this.input.keyboard.createCursorKeys();
        this.KeyW = this.input.keyboard.addKey("W");
        this.KeyA = this.input.keyboard.addKey("A");
        this.KeyS = this.input.keyboard.addKey("S");
        this.KeyD = this.input.keyboard.addKey("D");
        this.KeySpace = this.input.keyboard.addKey("Space");
        this.KeyCtrl = this.input.keyboard.addKey("CTRL");
    
        //  Some stars to collect, 12 in total, evenly spaced 70 pixels apart along the x axis
        
        for (let i = 0; i < 12; i++)
        {
            this.stars.push(this.matter.add.image(12 + 70 * i,0,"star"));
            this.stars[i].setBounce(Phaser.Math.FloatBetween(0.4, 0.8));
            this.stars[i].setFixedRotation();
        }
    
        //  The score
        this.scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px' }).setStroke("black",1);
    

        this.matter.world.on("collisionstart", function (event, bodyA, bodyB)
        {
            if(this.groundCheck(bodyA,bodyB) || this.groundCheck(bodyB,bodyA))
            {
                this.player["grounded"] = true;
            }
            
            this.collectStar(bodyA.gameObject,bodyB.gameObject);
            this.collectStar(bodyB.gameObject,bodyA.gameObject);
        },
        this);
         
        this.matter.world.on("collisionend", function (event, bodyA, bodyB)
        {
            if(this.groundCheck(bodyA,bodyB) || this.groundCheck(bodyB,bodyA))
                this.player["grounded"] = false;
        },
        this);
    }

    throwChain()
    {
        if(this.chain.length > 0)
            return;

        var prev =  this.matter.add.circle(this.player.x, this.player.y,5);
        var ballShape = this.matter.add.image(this.player.x, this.player.y,"rope");
            ballShape.setTint(0x888888);
            ballShape.setExistingBody(prev);
            ballShape.setCollisionCategory(0b0010);
            ballShape.setCollidesWith(0b0001);
            ballShape.setMass(10);
            ballShape.applyForce(new Phaser.Math.Vector2(this.throwStrength * this.throwDirection, -this.throwStrength));
            ballShape.setIgnoreGravity(true);
            ballShape.setFriction(0,0,0);
            ballShape.setOnCollide(()=>{
                this.makeChain(ballShape);
            });

        this.chain.push(ballShape);
    }

    makeChain(start : Phaser.Physics.Matter.Image)
    {
        start.setStatic(true);

        let distX : number = start.x - this.player.x;
        let distY : number = start.y - this.player.y;

        let distance : number = Phaser.Math.Distance.BetweenPoints(start.body.position,this.player.body.position)

        if(distance > this.maxChainLength)
        {
            this.destroyChain();
            return;
        }

        if(distance < this.minChainLength)
        {
            distance = this.minChainLength;
        }

        let ballCount : integer = distance / 24;

        var prev =  start.body;

        for (var i = 1; i < ballCount; i++)
        {


            var ball = this.getRopeBall(this.player.x + distX * (i/ballCount),this.player.y + distY * (i/ballCount));

            //@ts-ignore
            this.joints.push(this.matter.add.constraint(prev, ball.body, this.maxSegmentLength, 1));

            this.chain.push(ball);

            prev = ball.body;
        }

        //@ts-ignore
        this.joints.push(this.matter.add.constraint(prev, this.player.body, distance - this.joints.length*this.maxSegmentLength, 1));
        this.setChained(true);
    }

    getRopeBall(x:number,y:number)
    {
        var ballCollider = this.matter.add.circle(x,y,10);
        var ballShape = this.matter.add.image(x,y,"rope");
            ballShape.setTint(0x714c27);
            ballShape.setExistingBody(ballCollider);
            ballShape.setCollisionCategory(0b0010);
            ballShape.setCollidesWith(0b0001);
            ballShape.setMass(0.1);
        return ballShape;
    }

    destroyChain()
    {

        for (let i = 0; i < this.joints.length; i++) {
            this.matter.world.remove(this.joints[i]);
        }
        this.joints = [];

        for (let i = 0; i < this.chain.length; i++) {
            this.chain[i].destroy();
        }
        this.chain = [];

        this.setChained(false);
    }

    getChainLength()
    {
        //@ts-ignore
        return (this.joints.length - 1) * this.maxSegmentLength + this.joints[this.joints.length-1].length;
    }

    changeChainLength(change: number)
    {
        let length : number = this.getChainLength() + change;
        if(length > this.maxChainLength || length < this.minChainLength)
            return;

        //@ts-ignore
        this.joints[this.joints.length-1].length += change;

        //@ts-ignore
        if(this.joints[this.joints.length-1].length > this.maxSegmentLength)
        {
            //@ts-ignore
            length = this.joints[this.joints.length-1].length;
            //@ts-ignore
            this.joints[this.joints.length-1].length = this.maxSegmentLength;
            
            var ball = this.getRopeBall(this.player.x,this.player.y);
            //@ts-ignore
            this.joints[this.joints.length-1].bodyB = ball.body;

            this.chain.push(ball);
            
            //@ts-ignore
            this.joints.push(this.matter.add.constraint(ball.body, this.player.body, length - this.maxSegmentLength, 1));
            
        }
        //@ts-ignore
        else if(this.joints[this.joints.length-1].length <= 0)
        {
            //@ts-ignore
            length = this.joints[this.joints.length-1].length;
            this.matter.world.remove(this.joints[this.joints.length-1]);
            this.joints.splice(this.joints.length-1,1);
            //@ts-ignore
            this.joints[this.joints.length-1].bodyB = this.player.body;
            //@ts-ignore
            this.joints[this.joints.length-1].length = this.maxSegmentLength + length;
            this.chain[this.chain.length-1].destroy();
            this.chain.splice(this.chain.length-1,1);
        }
    }

    setChained(chained : boolean)
    {
        this.chained = chained;

        if(this.chained)
        {
            this.player.setFriction(0,0,0);
        }
        else
        {
            this.player.setFriction(0.1,0,1);
        }
        this.player["grounded"] = false;
    }

    groundCheck(player,ground) : boolean
    {
        return player.gameObject === this.player && this.platforms.indexOf(ground.gameObject) > -1;
    }

    update(time, delta)
    {
        if (this.gameOver)
        {
            return;
        }

        let trueDelta = delta/1000;

        if(!this.chained)
        {
            if (this.cursors.left.isDown || this.KeyA.isDown)
            {
                if(this.player.body.velocity.x > -this.playerSpeed)
                    this.player.setVelocityX(-this.playerSpeed);

                this.player.anims.play('left', true);
                this.throwDirection = -1;
            }
            else if (this.cursors.right.isDown || this.KeyD.isDown)
            {
                if(this.player.body.velocity.x < this.playerSpeed)
                    this.player.setVelocityX(this.playerSpeed);

                this.player.anims.play('right', true);
                this.throwDirection = 1;
            }
            else
            {
                this.player.anims.play('turn');
            }

            if ((this.cursors.up.isDown || this.KeyW.isDown) && this.player["grounded"] != null && this.player["grounded"] == true)
            {
                this.player.setVelocityY(-3.30*4);
            }

            if(this.KeySpace.isDown)
            {
                this.throwChain();
            }
        }
        else
        {
            if (this.cursors.left.isDown || this.KeyA.isDown)
            {
                this.player.thrustBack(this.playerTrust * trueDelta);

                this.player.anims.play('left', true);
                this.throwDirection = -1;
            }
            else if (this.cursors.right.isDown || this.KeyD.isDown)
            {
                this.player.thrust(this.playerTrust * trueDelta);

                this.player.anims.play('right', true);
                this.throwDirection = 1;
            }
            else
            {
                this.player.anims.play('turn');
            }

            

            if (this.cursors.up.isDown || this.KeyW.isDown)
            {
                this.changeChainLength(-this.ropingSpeed * trueDelta);
            }
            else if (this.cursors.down.isDown || this.KeyS.isDown)
            {
                this.changeChainLength(this.ropingSpeed * trueDelta);
            }

            if(this.KeyCtrl.isDown)
            {
                this.destroyChain();
            }
        }

        if(!this.chained && this.chain.length > 0)
        {
            let distance : number = Phaser.Math.Distance.BetweenPoints(this.chain[0].body.position,this.player.body.position)
            if(distance > this.maxChainLength)
            {
                this.destroyChain();
            }
        }
    }

    collectStar (player, star)
    {

        let index : number = this.stars.indexOf(star);

        if(index < 0 || !(player === this.player))
            return;
        
        this.stars[index].destroy();
        this.stars.splice(index,1);

        //  Add and update the score
        this.score += 10;
        this.scoreText.setText('Score: ' + this.score);

        if (this.stars.length <= 0)
        {
            
            for (let i = 0; i < 12; i++)
            {
                this.stars.push(this.matter.add.image(12 + 70 * i,0,"star"));
                this.stars[i].setBounce(Phaser.Math.FloatBetween(0.4, 0.8));
                this.stars[i].setFixedRotation();
            }

            var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);


        }
    }







    chainDemo()
    {

        this.matter.world.setBounds();

        var sides = 6;
        var size = 14;
        var distance = size * 4;
        var stiffness = 0.1;
        var lastPosition = new Phaser.Math.Vector2();
        var options = { friction: 0.005, frictionAir: 0, restitution: 1 };
        var pinOptions = { friction: 0, frictionAir: 0, restitution: 0, ignoreGravity: true, inertia: Infinity, isStatic: true };

        var current = null;
        var previous = null;

        this.input.on('pointerdown', function (pointer) {

            lastPosition.x = pointer.x;
            lastPosition.y = pointer.y;

            previous = this.matter.add.polygon(pointer.x, pointer.y, sides, size, pinOptions);

        }, this);

        this.input.on('pointermove', function (pointer) {

            if (pointer.isDown)
            {
                var x = pointer.x;
                var y = pointer.y;

                if (Phaser.Math.Distance.Between(x, y, lastPosition.x, lastPosition.y) > distance)
                {
                    lastPosition.x = x;
                    lastPosition.y = y;

                    current = this.matter.add.polygon(pointer.x, pointer.y, sides, size, options);

                    this.matter.add.constraint(previous, current, distance, stiffness);

                    previous = current;
                }
            }

        }, this);

        this.input.on('pointerup', function (pointer) {

            previous.isStatic = true;
            previous.ignoreGravity = true;

        }, this);
        
    }

}