define(["require", "exports", "./Tween"], function (require, exports, Tween_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Tweens {
        constructor(game) {
            this.tweens = Array();
            this.game = game;
        }
        update(time, delta) {
            let i = 0;
            while (i < this.tweens.length) {
                let element = this.tweens[i];
                element.update(time, delta);
                if (element.isDone()) {
                    this.tweens.splice(i, 1);
                    if (element.on_end) {
                        element.on_end(element);
                    }
                }
                else {
                    i += 1;
                }
            }
        }
        tween(target, property, targetValue, timeSecs, func, on_end) {
            let result = new Tween_1.default(target, property, targetValue, timeSecs, func, on_end);
            this.tweens.push(result);
            return result;
        }
        addTween(t) {
            this.tweens.push(t);
            return t;
            //return this.tween(t.target,t.property,t.targetValue,t.duration,t.func,t.on_end);
        }
        killTween(t) {
            let index = this.tweens.indexOf(t);
            if (index > -1) {
                this.tweens.splice(index, 1);
            }
        }
    }
    exports.default = Tweens;
});
//# sourceMappingURL=Tweens.js.map