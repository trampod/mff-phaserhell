import * as Phaser from "Phaser";
import Tween from "./Tweens/Tween";
import TweenGroup from "./Tweens/TweenGroup";
import Tweens from "./Tweens/Tweens";
import TweenSequence from "./Tweens/TweenSequence";
import TweenWait from "./Tweens/TweenWait";

export default class Play extends Phaser.Scene
{
    
    tw : Tweens;

    bricks: Phaser.Physics.Arcade.StaticGroup;
    paddle: Phaser.Physics.Arcade.Image;
    ball: Phaser.Physics.Arcade.Image;

    inputEnabled:boolean;

    constructor()
    {
        super("Play");
    }

    create()
    {
        console.log("Play.create()");

        this.tw = new Tweens(this.game);

        //  Enable world bounds, but disable the floor
        this.physics.world.setBoundsCollision(true, true, true, false);

        //  Create the bricks in a 10x6 grid
        this.bricks = this.physics.add.staticGroup({
            key: 'assets',
            frame: [ 'blue1', 'red1', 'green1', 'yellow1', 'silver1', 'purple1' ],
            frameQuantity: 10,
            gridAlign: { width: 10, height: 6, cellWidth: 64, cellHeight: 32, x: 112, y: 100 }
        });

        this.ball = this.physics.add.image(400, 500, 'assets', 'ball1').setCollideWorldBounds(true).setBounce(1);
        this.ball.setData('onPaddle', true);

        this.paddle = this.physics.add.image(400, 550, 'assets', 'paddle1').setImmovable();

        //  Our colliders

        //@ts-ignore
        this.physics.add.collider(this.ball, this.bricks, this.hitBrick, null, this);
        this.physics.add.collider(this.ball, this.paddle, this.hitPaddle, null, this);
        //this.physics.add.collider(this.ball, this.physics.world.checkCollision, this.hitWall, null, this);

        //  Input events
        this.input.on('pointermove', function (pointer)
        {

            //  Keep the paddle within the game
            this.paddle.x = Phaser.Math.Clamp(pointer.x, 52, 748);

            if (this.ball.getData('onPaddle'))
            {
                this.ball.x = this.paddle.x;
            }

        }, this);

        this.input.on('pointerup', function (pointer)
        {
            if (this.inputEnabled && this.ball.getData('onPaddle'))
            {
                this.ball.setVelocity(-75, -300);    
                this.ball.setData('onPaddle', false);
            }
        }, this);

        //@ts-ignore
        this.bricks.children.each(function (brick: Phaser.Physics.Arcade.Image)
        {
            brick.disableBody(true, true);
        });
        this.resetLevel();
    }

    hitBrick(ball: Phaser.Physics.Arcade.Sprite, brick: Phaser.Physics.Arcade.Sprite)
    {
        this.screenShake(0.3,0.1);

        brick.disableBody(false, false);
        
        brick.alpha = 0.5;
        let posX : number = brick.x;
        let posY : number = brick.y;
        let rot : number = brick.rotation;

        let duration : number = 1.5;

        let direction : number = Math.random() - 0.5;
        let directionFlat : number = direction > 0 ? 1 : -1;

        this.tw.addTween(new TweenGroup([
                new Tween(brick,"alpha",0,duration, Tween.smoothStop2),
                new Tween(brick,"y", posY + 250, duration, Tween.getBezier([0,-0.3,1])),
                new Tween(brick,"x", posX + (direction) * 30 + 20 * directionFlat, duration, Tween.linear),
                new Tween(brick,"rotation",rot+(direction) * 0.4 + directionFlat * 0.2, duration, Tween.smoothStop2)
            ],
            ()=>{
            brick.disableBody(true, true);

            brick.y = posY;
            brick.x = posX;
            brick.rotation = rot;
            if (this.bricks.countActive() === 0)
            {
                this.resetLevel();
            }
        }));
    }

    resetBall()
    {
        this.ball.setVelocity(0);
        this.ball.setPosition(this.paddle.x, 500);
        this.ball.setData('onPaddle', true);
    }

    resetLevel()
    {
        this.resetBall();
        this.inputEnabled = false;

        let seq : TweenSequence = new TweenSequence(()=>{this.inputEnabled = true});

        //@ts-ignore
        this.bricks.children.each(function (brick: Phaser.Physics.Arcade.Image)
        {
            brick.enableBody(false, 0, 0, true, true)
            
            let posY : number = brick.y;
            
            brick.y = posY -100;
            brick.alpha = 0;
            seq.join(new TweenSequence()
                .append(new TweenWait(Math.random()))
                .append(new Tween(brick,"alpha",1,1,Tween.linear))
                .join(new Tween(brick,"y",posY,1,Tween.easeOutBounce)));
        });

        this.tw.addTween(seq);
    }

    hitPaddle(ball, paddle)
    {
        var diff = 0;

        let direction : number;

        if (ball.x < paddle.x)
        {
            //  Ball is on the left-hand side of the paddle
            diff = paddle.x - ball.x;
            ball.setVelocityX(-10 * diff);
            direction = -1;
        }
        else if (ball.x > paddle.x)
        {
            //  Ball is on the right-hand side of the paddle
            diff = ball.x -paddle.x;
            ball.setVelocityX(10 * diff);
            direction = 1;
        }
        else
        {
            //  Ball is perfectly in the middle
            //  Add a little random X to stop it bouncing straight up!
            direction = Math.random() > 0.5 ? -1 : 1;
            ball.setVelocityX((2 + Math.random() * 8) * direction);
        }
        this.tw.addTween(this.simpleShake(paddle,-0.08 * direction, 0.2));
        // this.screenShake(0.2,0.1);
    }

    screenShake(intensity:number = 1, duration:number = 0.3, count : number = 4)
    {
        let seq : TweenSequence = new TweenSequence();

        let direction : number = Math.random() > 0.5 ? -1 : 1;

        for (let i = 0; i < count; i++) {
            seq.append(this.simpleShake(this.cameras.main,0.05*intensity * direction, duration/count));
        }

        this.tw.addTween(seq);
    }

    simpleShake(target: any, angle:number, time: number, property:string = "rotation") : TweenSequence
    {
        return TweenSequence.createSequence([
            new Tween(target,property,angle,time/4,Tween.linear),
            new Tween(target,property,-angle,time/2,Tween.linear),
            new Tween(target,property,0,time/4,Tween.linear)
        ]);
    }

    update(time: number, delta: number)
    {
        this.tw.update(time, delta);   
        if (this.ball.y > 600)
        {
            this.resetBall();
        }
    }

}