import * as Phaser from "Phaser";
import Tween from "./Tween";

export default class Tweens {

    private game : Phaser.Game;
    private tweens : Array<Tween> = Array<Tween>();

    public constructor(game: Phaser.Game)
    {
        this.game = game;
    }

    public update(time: number, delta: number)
    {
        let i: number = 0;
        while (i < this.tweens.length) {
            let element : Tween = this.tweens[i];
            element.update(time, delta);
            if (element.isDone()) {
                this.tweens.splice(i, 1);
                if (element.on_end) {
                    element.on_end(element);
                }
            } else {
                i += 1;
            }
        }
    }
    
    public tween(target, property: string, targetValue: number, timeSecs: number, func : (t: number) => number, on_end : (t: Tween) => void) : Tween
    {
        let result : Tween = new Tween(target, property, targetValue, timeSecs, func, on_end);
        this.tweens.push(result);
        return result;
    }

    public addTween(t : Tween) : Tween
    {
        this.tweens.push(t);
        return t;
        //return this.tween(t.target,t.property,t.targetValue,t.duration,t.func,t.on_end);
    }

    public killTween(t : Tween)
    {
        let index = this.tweens.indexOf(t);
        if (index > -1)
        {
            this.tweens.splice(index, 1);
        }
    }
}