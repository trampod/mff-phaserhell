import * as Phaser from "Phaser";
import Tween from "./Tween";
import TweenGroup from "./TweenGroup";
import Tweens from "./Tweens";
import TweenWait from "./TweenWait";

/// This class should work similary to DOTween sequence, to be more familiar and useful
export default class TweenSequence extends Tween
{
	tweenList : [number,TweenGroup][];

    public constructor(on_end: (t: Tween) => void = null)
    {
		super(null, null, null, 0, null, on_end);
		this.tweenList = [[0,new TweenGroup([new TweenWait(0)],null)]];
    }


    public update(time: number, deltaMillis: number)
	{
        if(this.tweenList.length == 0)
            return;

        let oldTime : number = this.timeSecs;
        this.timeSecs += deltaMillis / 1000;

        let element : [number,Tween] = this.tweenList[0];

        while (element[0] < this.timeSecs)
        {
            if(element[0] < oldTime)
                element[1].update(time, deltaMillis);
            else
                element[1].update(element[0], this.timeSecs - element[0])

            if (element[1].isDone())
            {
                this.tweenList.splice(0, 1);

                if (element[1].on_end)
                    element[1].on_end(element[1]);

                if(this.isDone())
                    return;
                
                element = this.tweenList[0];
                element[1].refreshStartValue();
            }
            else
            {
                return;
            }
        }
    }

    public refreshStartValue()
    {
        if(this.tweenList.length > 0)
            this.tweenList[0][1].refreshStartValue();
    }

    
    public isDone() : boolean
	{
        return this.tweenList.length == 0;
    }


    public setOnEnd(on_end: (t: Tween) => void = null)
    {
        this.on_end = on_end;
    }

    public append(t:Tween) : TweenSequence
    {
		this.tweenList.push([this.duration,new TweenGroup([t],null)]);
        this.duration += t.duration;

        return this;
    }

    public join(t:Tween) : TweenSequence
    {
        let index : number = this.tweenList.length-1;
        this.tweenList[index][1].addTween(t);
        this.duration = this.tweenList[index][0] + this.tweenList[index][1].duration;

        return this;
    }

    public break() : TweenSequence
    {
        this.append(new TweenWait(0));

        return this;
    }

    public static createSequence(tweens : Tween[],on_end: (t: Tween) => void = null) : TweenSequence
    {
        let seq : TweenSequence = new TweenSequence(on_end);

		for(var tween of tweens)
		{
            seq.append(tween);
		}

        return seq;
    }
}