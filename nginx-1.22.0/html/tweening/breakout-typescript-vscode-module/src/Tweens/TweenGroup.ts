import * as Phaser from "Phaser";
import Tween from "./Tween";


export default class TweenGroup extends Tween
{
	tweenList : Tween[];

    public constructor(tweens : Tween[], on_end: (t: Tween) => void = null)
    {
		super(null, null, null, 0, null, on_end);
        if(tweens != null)
    		this.tweenList = tweens;
        else
            this.tweenList = [];
            
		for(var tween of this.tweenList)
		{
			if(tween.duration > this.duration)
			{
				this.duration = tween.duration;
			}
		}
    }

    public refreshStartValue()
    {
		for(var tween of this.tweenList)
		{
			tween.refreshStartValue();
		}
    }


    public update(time: number, deltaMillis: number)
	{
        let i: number = 0;
        while (i < this.tweenList.length) {
            let element : Tween = this.tweenList[i];
            element.update(time, deltaMillis);
            if (element.isDone()) {
                this.tweenList.splice(i, 1);
                if (element.on_end) {
                    element.on_end(element);
                }
            } else {
                i += 1;
            }
        }
    }

    
    public isDone() : boolean
	{
        return this.tweenList.length == 0;
    }

    public addTween(t : Tween)
    {
        this.tweenList.push(t);
    }
}