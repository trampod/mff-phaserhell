var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./Tween"], function (require, exports, Tween_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TweenGroup = /** @class */ (function (_super) {
        __extends(TweenGroup, _super);
        function TweenGroup(duration, on_end) {
            if (on_end === void 0) { on_end = null; }
            return _super.call(this, null, null, null, duration, null, on_end) || this;
        }
        TweenGroup.prototype.update = function (time, deltaMillis) {
            this.timeSecs += deltaMillis / 1000;
        };
        TweenGroup.prototype.isDone = function () {
            return this.timeSecs >= this.duration;
        };
        return TweenGroup;
    }(Tween_1.default));
    exports.default = TweenGroup;
});
//# sourceMappingURL=TweenWait.js.map