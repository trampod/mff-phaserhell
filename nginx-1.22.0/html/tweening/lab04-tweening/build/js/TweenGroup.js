var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./Tween"], function (require, exports, Tween_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TweenGroup = /** @class */ (function (_super) {
        __extends(TweenGroup, _super);
        function TweenGroup(tweens, on_end) {
            if (on_end === void 0) { on_end = null; }
            var _this = _super.call(this, null, null, null, 0, null, on_end) || this;
            if (tweens != null)
                _this.tweenList = tweens;
            else
                _this.tweenList = [];
            for (var _i = 0, _a = _this.tweenList; _i < _a.length; _i++) {
                var tween = _a[_i];
                if (tween.duration * tween.repeat > _this.duration) {
                    _this.duration = tween.duration * tween.repeat;
                }
            }
            return _this;
        }
        TweenGroup.prototype.refreshStartValue = function () {
            for (var _i = 0, _a = this.tweenList; _i < _a.length; _i++) {
                var tween = _a[_i];
                tween.refreshStartValue();
            }
        };
        TweenGroup.prototype.update = function (time, deltaMillis) {
            var i = 0;
            while (i < this.tweenList.length) {
                var element = this.tweenList[i];
                element.update(time, deltaMillis);
                if (element.isDone()) {
                    this.tweenList.splice(i, 1);
                    if (element.on_end) {
                        element.on_end(element);
                    }
                }
                else {
                    i += 1;
                }
            }
        };
        TweenGroup.prototype.isDone = function () {
            return this.tweenList.length == 0;
        };
        TweenGroup.prototype.addTween = function (t) {
            this.tweenList.push(t);
        };
        return TweenGroup;
    }(Tween_1.default));
    exports.default = TweenGroup;
});
//# sourceMappingURL=TweenGroup.js.map