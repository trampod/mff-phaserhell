define(["require", "exports", "./Tween"], function (require, exports, Tween_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Tweens = /** @class */ (function () {
        function Tweens(game) {
            this.tweens = Array();
            this.game = game;
        }
        Tweens.prototype.update = function (time, delta) {
            var i = 0;
            while (i < this.tweens.length) {
                var element = this.tweens[i];
                element.update(time, delta);
                if (element.isDone()) {
                    this.tweens.splice(i, 1);
                    if (element.on_end) {
                        element.on_end(element);
                    }
                }
                else {
                    i += 1;
                }
            }
        };
        Tweens.prototype.tween = function (target, property, targetValue, timeSecs, func, on_end) {
            var result = new Tween_1.default(target, property, targetValue, timeSecs, func, on_end);
            this.tweens.push(result);
            return result;
        };
        Tweens.prototype.addTween = function (t) {
            this.tweens.push(t);
            return t;
            //return this.tween(t.target,t.property,t.targetValue,t.duration,t.func,t.on_end);
        };
        Tweens.prototype.killTween = function (t) {
            var index = this.tweens.indexOf(t);
            if (index > -1) {
                this.tweens.splice(index, 1);
            }
        };
        return Tweens;
    }());
    exports.default = Tweens;
});
//# sourceMappingURL=Tweens.js.map