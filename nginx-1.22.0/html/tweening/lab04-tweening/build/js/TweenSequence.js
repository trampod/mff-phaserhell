var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./Tween", "./TweenGroup", "./TweenWait"], function (require, exports, Tween_1, TweenGroup_1, TweenWait_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /// This class should work similary to DOTween sequence, to be more familiar and useful
    var TweenSequence = /** @class */ (function (_super) {
        __extends(TweenSequence, _super);
        function TweenSequence(on_end) {
            if (on_end === void 0) { on_end = null; }
            var _this = _super.call(this, null, null, null, 0, null, on_end) || this;
            _this.tweenList = [[0, new TweenGroup_1.default([new TweenWait_1.default(0)], null)]];
            return _this;
        }
        TweenSequence.prototype.update = function (time, deltaMillis) {
            if (this.tweenList.length == 0)
                return;
            var oldTime = this.timeSecs;
            this.timeSecs += deltaMillis / 1000;
            var element = this.tweenList[0];
            while (element[0] < this.timeSecs) {
                if (element[0] < oldTime)
                    element[1].update(time, deltaMillis);
                else
                    element[1].update(element[0], this.timeSecs - element[0]);
                if (element[1].isDone()) {
                    this.tweenList.splice(0, 1);
                    if (element[1].on_end)
                        element[1].on_end(element[1]);
                    if (this.isDone())
                        return;
                    element = this.tweenList[0];
                    element[1].refreshStartValue();
                }
                else {
                    return;
                }
            }
        };
        TweenSequence.prototype.refreshStartValue = function () {
            if (this.tweenList.length > 0)
                this.tweenList[0][1].refreshStartValue();
        };
        TweenSequence.prototype.isDone = function () {
            return this.tweenList.length == 0;
        };
        TweenSequence.prototype.setOnEnd = function (on_end) {
            if (on_end === void 0) { on_end = null; }
            this.on_end = on_end;
        };
        TweenSequence.prototype.append = function (t) {
            this.tweenList.push([this.duration, new TweenGroup_1.default([t], null)]);
            this.duration += t.duration * t.repeat;
            return this;
        };
        TweenSequence.prototype.join = function (t) {
            var index = this.tweenList.length - 1;
            this.tweenList[index][1].addTween(t);
            this.duration = this.tweenList[index][0] + this.tweenList[index][1].duration;
            return this;
        };
        TweenSequence.prototype.break = function () {
            this.append(new TweenWait_1.default(0));
            return this;
        };
        TweenSequence.createSequence = function (tweens, on_end) {
            if (on_end === void 0) { on_end = null; }
            var seq = new TweenSequence(on_end);
            for (var _i = 0, tweens_1 = tweens; _i < tweens_1.length; _i++) {
                var tween = tweens_1[_i];
                seq.append(tween);
            }
            return seq;
        };
        return TweenSequence;
    }(Tween_1.default));
    exports.default = TweenSequence;
});
//# sourceMappingURL=TweenSequence.js.map