define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Tween = /** @class */ (function () {
        function Tween(target, property, targetValue, timeSecs, func, on_end, repeat) {
            if (on_end === void 0) { on_end = null; }
            if (repeat === void 0) { repeat = 1; }
            this.target = target;
            this.property = property;
            // so the sequences and groups work
            if (this.target && this.property)
                this.startValue = this.target[this.property];
            this.targetValue = targetValue;
            this.timeSecs = 0;
            this.duration = timeSecs;
            this.func = func;
            this.on_end = on_end;
            this.repeat = repeat;
        }
        Tween.prototype.refreshStartValue = function () {
            if (this.target && this.property)
                this.startValue = this.target[this.property];
        };
        Tween.prototype.update = function (time, deltaMillis) {
            // TODO: apply the tween correctly
            this.timeSecs += deltaMillis / 1000;
            this.target[this.property] = this.applyContext(this.func(this.convertT(this.timeSecs)));
        };
        Tween.prototype.applyContext = function (x) {
            return this.startValue * (1 - x) + this.targetValue * x;
        };
        Tween.prototype.convertT = function (t) {
            return this.timeSecs / this.duration;
        };
        Tween.prototype.isDone = function () {
            // TODO: after you adapt update(), check if this method still correctly
            //       reports your tween has finished
            var result = this.timeSecs >= this.duration;
            if (result) {
                this.repeat--;
                if (this.repeat <= 0) {
                    this.timeSecs = this.duration;
                }
                else {
                    this.timeSecs -= this.duration;
                }
                this.update(0, 0);
            }
            return this.timeSecs >= this.duration;
        };
        // -------------------------------------
        // FOLLOWS THE STASH OF EASING FUNCTIONS
        // -------------------------------------
        Tween.linear = function (t) {
            return t;
        };
        Tween.smoothStart2 = function (t) {
            return t * t;
        };
        Tween._smoothStartN = function (t, n) {
            var tmp = 1;
            for (var i = 0; i < n; i++) {
                tmp *= t;
            }
            return tmp;
        };
        Tween.getSmoothStartN = function (n) {
            //if(Number.isInteger(n))
            var _this = this;
            if (n == Math.floor(n))
                return function (t) { return _this._smoothStartN(t, n); };
            return function (t) { return _this.linearBezie(_this._smoothStartN(t, n), _this._smoothStartN(t, n + 1), n - Math.floor(n)); };
        };
        Tween.smoothStop2 = function (t) {
            var d = 1 - t;
            var g = Tween.smoothStart2(d);
            return 1 - g;
        };
        Tween._smoothStopN = function (t, n) {
            return (1 - t) * this._smoothStartN(1 - t, n);
        };
        Tween.getSmoothStopN = function (n) {
            //if(Number.isInteger(n))
            var _this = this;
            if (n == Math.floor(n))
                return function (t) { return _this._smoothStopN(t, n); };
            return function (t) { return _this.linearBezie(_this._smoothStopN(t, n), _this._smoothStopN(t, n + 1), n - Math.floor(n)); };
        };
        Tween.smoothStep3 = function (t) {
            return this.linearBezie(Tween.smoothStart2(t), Tween.smoothStop2(t), t);
        };
        Tween.hesitate = function (t) {
            return this.cubicBezie(0, 1, 0, 1, t);
        };
        Tween.arch2 = function (t) {
            return t * (1 - t);
        };
        Tween.smoothStartArch3 = function (t) {
            return t * t * (1 - t);
        };
        Tween.smoothStopArch2 = function (t) {
            return t * (1 - t) * (1 - t);
        };
        Tween.smoothStepArch2 = function (t) {
            return t * t * (1 - t) * (1 - t);
        };
        Tween.bellCurve6 = function (t) {
            return this._smoothStartN(3, t) * this._smoothStopN(3, t);
        };
        Tween.bounceTop = function (x) {
            return Math.abs(x);
        };
        Tween.bounceBottom = function (x) {
            return 1 - Math.abs(1 - x);
        };
        Tween.bounceTopBottom = function (x) {
            return Tween.bounceTop(Tween.bounceBottom(x));
        };
        Tween.linearBezie = function (start, end, t) {
            return (1 - t) * start + t * end;
        };
        Tween.quadraticBezie = function (start, mid, end, t) {
            return (1 - t) * (1 - t) * start + (1 - t) * t * mid + t * t * end;
        };
        Tween.cubicBezie = function (start, mid1, mid2, end, t) {
            return (1 - t) * (1 - t) * (1 - t) * start + (1 - t) * (1 - t) * t * mid1 + (1 - t) * t * t * mid2 + t * t * t * end;
        };
        Tween.BezierN = function (points, t) {
            if (points.length < 2)
                return 0;
            if (points.length == 2)
                return this.linearBezie(points[0], points[1], t);
            var i;
            var newPoints = [];
            for (i = 1; i < points.length; i++) {
                newPoints.push(this.linearBezie(points[i - 1], points[i], t));
            }
            return this.BezierN(newPoints, t);
        };
        Tween.getBezier = function (points) {
            var _this = this;
            return function (t) { return _this.BezierN(points, t); };
        };
        Tween.funkyBezier = function (x) {
            //return Tween.cubicBezie(0,1.5,0.2,1,x);
            return Tween.BezierN([0, 1.5, 0.2, 2, 1.3, 0, -1, 3, 2, 2, 0.2, 1, 0, 1], x);
        };
        // TODO: add more easing functions here
        //Following easing functions are from https://easings.net/
        Tween.easeInElastic = function (x) {
            var c4 = (2 * Math.PI) / 3;
            return x == 0
                ? 0
                : x == 1
                    ? 1
                    : -Math.pow(2, 10 * x - 10) * Math.sin((x * 10 - 10.75) * c4);
        };
        Tween.easeOutElastic = function (x) {
            var c4 = (2 * Math.PI) / 3;
            return x === 0
                ? 0
                : x === 1
                    ? 1
                    : Math.pow(2, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
        };
        Tween.easeInOutElastic = function (x) {
            var c5 = (2 * Math.PI) / 4.5;
            return x === 0
                ? 0
                : x === 1
                    ? 1
                    : x < 0.5
                        ? -(Math.pow(2, 20 * x - 10) * Math.sin((20 * x - 11.125) * c5)) / 2
                        : (Math.pow(2, -20 * x + 10) * Math.sin((20 * x - 11.125) * c5)) / 2 + 1;
        };
        Tween.easeInBounce = function (x) {
            return 1 - Tween.easeOutBounce(1 - x);
        };
        Tween.easeOutBounce = function (x) {
            var n1 = 7.5625;
            var d1 = 2.75;
            if (x < 1 / d1) {
                return n1 * x * x;
            }
            else if (x < 2 / d1) {
                return n1 * (x -= 1.5 / d1) * x + 0.75;
            }
            else if (x < 2.5 / d1) {
                return n1 * (x -= 2.25 / d1) * x + 0.9375;
            }
            else {
                return n1 * (x -= 2.625 / d1) * x + 0.984375;
            }
        };
        Tween.easeInOutBounce = function (x) {
            return x < 0.5
                ? (1 - Tween.easeOutBounce(1 - 2 * x)) / 2
                : (1 + Tween.easeOutBounce(2 * x - 1)) / 2;
        };
        return Tween;
    }());
    exports.default = Tween;
});
//# sourceMappingURL=Tween.js.map