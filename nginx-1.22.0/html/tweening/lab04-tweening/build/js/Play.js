var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "Phaser", "./Tween", "./Tweens", "./TweenSequence"], function (require, exports, Phaser, Tween_1, Tweens_1, TweenSequence_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Play = /** @class */ (function (_super) {
        __extends(Play, _super);
        function Play() {
            return _super.call(this, "Play") || this;
        }
        Play.prototype.create = function () {
            console.log("Play.create()");
            this.tw = new Tweens_1.default(this.game);
            this.sprite1 = this.add.sprite(160, 120, "playership1").setOrigin(0.5, 0.5).setAlpha(1);
            this.sprite2 = this.add.sprite(480, 120, "playership2").setOrigin(0.5, 0.5).setAlpha(1);
            this.sprite3 = this.add.sprite(160, 360, "playership3").setOrigin(0.5, 0.5).setAlpha(1);
            this.sprite4 = this.add.sprite(480, 360, "enemy").setOrigin(0.5, 0.5).setAlpha(1);
            // Enables movement of player with WASD keys
            this.input.keyboard.on('keydown-ONE', function (event) {
                this.scene.button1();
            });
            this.input.keyboard.on('keydown-TWO', function (event) {
                this.scene.button2();
            });
            this.input.keyboard.on('keydown-THREE', function (event) {
                this.scene.button3();
            });
            this.input.keyboard.on('keydown-FOUR', function (event) {
                this.scene.button4();
            });
            this.input.keyboard.on('keydown-R', function (event) {
                this.scene.restart();
            });
        };
        Play.prototype.update = function (time, delta) {
            this.tw.update(time, delta);
        };
        Play.prototype.button1 = function () {
            // TODO: some showcase on this.sprite2 using SmoothStepN on Alpha
            this.sprite1.alpha = 0;
            console.log("something");
            this.tw.tween(this.sprite1, "alpha", 1, 1, Tween_1.default.smoothStart2, null);
        };
        Play.prototype.button2 = function () {
            // TODO: some showcase on this.sprite2 using funky bezier on Alpha, Rotation and Position
            this.sprite2.x = -100;
            this.sprite2.y = -100;
            this.sprite2.alpha = 0;
            this.sprite2.rotation = 360;
            // this.tw.tween(this.sprite2, "x", 480, 1, Tween.easeOutElastic, null);
            // this.tw.tween(this.sprite2, "y", 120, 1, Tween.easeOutElastic, null);
            this.tw.tween(this.sprite2, "x", 480, 1, Tween_1.default.funkyBezier, null);
            this.tw.tween(this.sprite2, "y", 120, 1, Tween_1.default.funkyBezier, null);
            this.tw.tween(this.sprite2, "alpha", 1, 1, Tween_1.default.funkyBezier, null);
            this.tw.tween(this.sprite2, "rotation", 0, 1, Tween_1.default.funkyBezier, null);
        };
        Play.prototype.button3 = function () {
            // TODO: some showcase on this.sprite3 using spline on beziers utilizing on_end
            var _this = this;
            this.sprite3.x = 160;
            this.sprite3.y = 360;
            //this.tw.tween(this.sprite3, "x", 1, 1, Tween.linear, null);
            // this.sprite3.x = -100;
            // this.sprite3.y = -100;
            this.tw.tween(this.sprite3, "x", 210, 1, Tween_1.default.getBezier([0, 0, 1]), function () {
                _this.tw.tween(_this.sprite3, "x", 260, 1, Tween_1.default.getBezier([0, 1, 1]), function () {
                    _this.tw.tween(_this.sprite3, "x", 160, 2, Tween_1.default.getBezier([0, 0, 1, 1]), null);
                });
            });
            this.tw.tween(this.sprite3, "y", 310, 1, Tween_1.default.getBezier([0, 0, 1, 1]), function () {
                _this.tw.tween(_this.sprite3, "y", 360, 1, Tween_1.default.getBezier([0, 0, 1, 1]), function () {
                    _this.tw.tween(_this.sprite3, "y", 310, 2, Tween_1.default.getBezier([0, 1, 0]), null);
                });
            });
            /*
    
            let seq : TweenSequence = new TweenSequence();
    
            seq.join(new Tween(this.sprite3, "x", 260, 2, Tween.smoothStop2, null));
            seq.join(new Tween(this.sprite3, "y", 260, 1, Tween.smoothStop2, null));
            seq.append(new Tween(this.sprite3, "y", 60, 2, Tween.linear, null));
            seq.join(TweenSequence.createSequence(
                    [
                        new Tween(this.sprite3, "x", 210, 0.5, Tween.linear, null),
                        new Tween(this.sprite3, "x", 310, 1, Tween.linear, null),
                        new Tween(this.sprite3, "x", 260, 0.5, Tween.linear, null)
                    ]));
            
            seq.append(new Tween(this.sprite3, "alpha", 0.5, 1, Tween.easeInOutElastic, null));
    
            seq.append(new Tween(this.sprite3, "x", 300, 0.3, Tween.smoothStart2, null));
            seq.join(new Tween(this.sprite3, "y", 100, 0.3, Tween.smoothStart2, null));
            seq.join(new Tween(this.sprite3, "rotation", Math.PI/2, 0.3, Tween.smoothStart2, null));
            
            seq.append(new Tween(this.sprite3, "x", 260, 0.3, Tween.linear, null));
            seq.join(new Tween(this.sprite3, "y", 140, 0.3, Tween.linear, null));
            seq.join(new Tween(this.sprite3, "rotation",  Math.PI, 0.3, Tween.linear, null));
            
            seq.append(new Tween(this.sprite3, "x", 220, 0.3, Tween.linear, null));
            seq.join(new Tween(this.sprite3, "y", 100, 0.3, Tween.linear, null));
            seq.join(new Tween(this.sprite3, "rotation",  Math.PI /2, 0.3, Tween.linear, null));
            
            seq.append(new Tween(this.sprite3, "x", 260, 0.3, Tween.smoothStop2, null));
            seq.join(new Tween(this.sprite3, "y", 60, 0.3, Tween.smoothStop2, null));
            seq.join(new Tween(this.sprite3, "rotation", 0, 0.3, Tween.smoothStop2, null));
            
            seq.append(new Tween(this.sprite3, "alpha", 1, 1, Tween.easeInOutElastic, null));
            
            seq.append(new Tween(this.sprite3, "x", 160, 1, Tween.smoothStop2, null));
            seq.join(new Tween(this.sprite3, "y", 360, 1, Tween.smoothStop2, null));
    
    
            this.tw.addTween(seq);
    
            */
            /*
    
            let tweens : Tween[] =
            [
                new TweenGroup(
                [
                    new Tween(this.sprite3, "x", 260, 2, Tween.smoothStop2, null),
                    new Tween(this.sprite3, "y", 260, 1, Tween.smoothStop2, null)
                ]
                ,null),
                new TweenGroup(
                [
                    new TweenSequence(
                    [
                        new Tween(this.sprite3, "x", 210, 0.5, Tween.linear, null),
                        new Tween(this.sprite3, "x", 310, 1, Tween.linear, null),
                        new Tween(this.sprite3, "x", 260, 0.5, Tween.linear, null)
                    ],null),
                    new Tween(this.sprite3, "y", 60, 2, Tween.smoothStop2, null)
                ]
                ,null),
                new TweenSequence(
                [
                    new Tween(this.sprite3, "alpha", 0.5, 1, Tween.easeInOutElastic, null),
                    new TweenGroup(
                    [
                        new Tween(this.sprite3, "x", 300, 0.3, Tween.smoothStart2, null),
                        new Tween(this.sprite3, "y", 100, 0.3, Tween.smoothStart2, null),
                        new Tween(this.sprite3, "rotation", Math.PI/2, 0.3, Tween.smoothStart2, null)
                    ]
                    ,null),
                    new TweenGroup(
                    [
                        new Tween(this.sprite3, "x", 260, 0.3, Tween.linear, null),
                        new Tween(this.sprite3, "y", 140, 0.3, Tween.linear, null),
                        new Tween(this.sprite3, "rotation",  Math.PI, 0.3, Tween.linear, null)
                    ]
                    ,null),
                    new TweenGroup(
                    [
                        new Tween(this.sprite3, "x", 220, 0.3, Tween.linear, null),
                        new Tween(this.sprite3, "y", 100, 0.3, Tween.linear, null),
                        new Tween(this.sprite3, "rotation",  Math.PI /2, 0.3, Tween.linear, null)
                    ]
                    ,null),
                    new TweenGroup(
                    [
                        new Tween(this.sprite3, "x", 260, 0.3, Tween.smoothStop2, null),
                        new Tween(this.sprite3, "y", 60, 0.3, Tween.smoothStop2, null),
                        new Tween(this.sprite3, "rotation", 0, 0.3, Tween.smoothStop2, null)
                    ]
                    ,null),
                    new Tween(this.sprite3, "alpha", 1, 1, Tween.easeInOutElastic, null)
                ]
                ,null),
                new TweenGroup(
                [
                    new Tween(this.sprite3, "x", 160, 1, Tween.smoothStop2, null),
                    new Tween(this.sprite3, "y", 360, 1, Tween.smoothStop2, null)
                ]
                ,null)
            ];
    
            this.tw.addTween(new TweenSequence(tweens,null));
    
            */
            // this.tw.addTween(new TweenSequence(
            //     [
            //         new Tween(this.sprite3, "x", 260, 1, Tween.smoothStop2, null),
            //         new Tween(this.sprite3, "x", 360, 1, Tween.smoothStop2, null),
            //         new Tween(this.sprite3, "x", 160, 1, Tween.smoothStop2, null)
            //     ],null));
        };
        Play.prototype.button4 = function () {
            // TODO: some showcase some smooth spline movement, change position of this.sprite4
            //       the spline must be built using single line of code, either using varargs
            //       or the Builder pattern
            this.sprite4.x = 480;
            this.sprite4.y = 360;
            //I see this as a single line of code, since only one ; was used
            this.tw.addTween(new TweenSequence_1.default()
                .append(new Tween_1.default(this.sprite4, "x", 160, 3, Tween_1.default.getBezier([0, 1, 2, 1, -1, 0]), null, 3))
                .join(new Tween_1.default(this.sprite4, "y", 100, 3, Tween_1.default.getBezier([0, 0.1, 0.8, 1, 1, 1, 2, 2, 1, 0.5, -0.1, 0]), null, 3)));
            //this.tw.addTween(new TweenSequence().append(new Tween(this.sprite4, "x", -100, 3, Tween.linear, null)).join(new Tween(this.sprite4, "y", -100, 3, Tween.linear, null)));
        };
        Play.prototype.restart = function () {
            this.scene.restart();
        };
        return Play;
    }(Phaser.Scene));
    exports.default = Play;
});
//# sourceMappingURL=Play.js.map