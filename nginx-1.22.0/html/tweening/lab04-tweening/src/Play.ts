import * as Phaser from "Phaser";
import Tween from "./Tween";
import TweenGroup from "./TweenGroup";
import Tweens from "./Tweens";
import TweenSequence from "./TweenSequence";

export default class Play extends Phaser.Scene {

    tw : Tweens;

    sprite1: Phaser.GameObjects.Sprite;

    sprite2: Phaser.GameObjects.Sprite;

    sprite3: Phaser.GameObjects.Sprite;

    sprite4: Phaser.GameObjects.Sprite;   

    constructor() {
        super("Play");
    }

    create() {
        console.log("Play.create()");

        this.tw = new Tweens(this.game);

        this.sprite1 = this.add.sprite(160, 120, "playership1").setOrigin(0.5, 0.5).setAlpha(1);
        this.sprite2 = this.add.sprite(480, 120, "playership2").setOrigin(0.5, 0.5).setAlpha(1);
        this.sprite3 = this.add.sprite(160, 360, "playership3").setOrigin(0.5, 0.5).setAlpha(1);
        this.sprite4 = this.add.sprite(480, 360, "enemy").setOrigin(0.5, 0.5).setAlpha(1);                

        // Enables movement of player with WASD keys
        this.input.keyboard.on('keydown-ONE', function (event: object) {
            this.scene.button1();
        });
        this.input.keyboard.on('keydown-TWO', function (event: object) {
            this.scene.button2();
        });
        this.input.keyboard.on('keydown-THREE', function (event: object) {
            this.scene.button3();
        });
        this.input.keyboard.on('keydown-FOUR', function (event: object) {
            this.scene.button4();
        }); 
        this.input.keyboard.on('keydown-R', function (event: object) {
            this.scene.restart();
        });                   
    }

    update(time: number, delta: number) {     
        this.tw.update(time, delta);   
    }    

    button1() {
        // TODO: some showcase on this.sprite2 using SmoothStepN on Alpha
        this.sprite1.alpha = 0;
        console.log("something");
        this.tw.tween(this.sprite1, "alpha", 1, 1, Tween.smoothStart2, null);
    }

    button2() {
        // TODO: some showcase on this.sprite2 using funky bezier on Alpha, Rotation and Position
        this.sprite2.x = -100;
        this.sprite2.y = -100;
        this.sprite2.alpha = 0;
        this.sprite2.rotation = 360;
        
        this.tw.tween(this.sprite2, "x", 480, 1, Tween.funkyBezier, null);
        this.tw.tween(this.sprite2, "y", 120, 1, Tween.funkyBezier, null);
        this.tw.tween(this.sprite2, "alpha", 1, 1, Tween.funkyBezier, null);
        this.tw.tween(this.sprite2, "rotation", 0, 1, Tween.funkyBezier, null);
        
    }

    button3() {
        // TODO: some showcase on this.sprite3 using spline on beziers utilizing on_end

        
        this.sprite3.x = 160;
        this.sprite3.y = 360;

        this.tw.tween(this.sprite3, "x", 210,1,Tween.getBezier([0,0,1]),()=>{
            this.tw.tween(this.sprite3, "x", 260,1,Tween.getBezier([0,1,1]),()=>{
                this.tw.tween(this.sprite3, "x", 160, 2, Tween.getBezier([0,0,1,1]),null)
            })
        });
        this.tw.tween(this.sprite3, "y",310,1,Tween.getBezier([0,0,1,1]),()=>{
            this.tw.tween(this.sprite3, "y", 360, 1, Tween.getBezier([0,0,1,1]), ()=>{
                this.tw.tween(this.sprite3, "y", 310, 2,Tween.getBezier([0,1,0]),null)
            })
        });
    }

    button4() {    
        // TODO: some showcase some smooth spline movement, change position of this.sprite4
        //       the spline must be built using single line of code, either using varargs
        //       or the Builder pattern
        
        this.sprite4.x = 480;
        this.sprite4.y = 360;

        //I see this as a single line of code, since only one ; was used
        this.tw.addTween(new TweenSequence()
            .append(new Tween(this.sprite4, "x", 160, 3, Tween.getBezier([0,1,2,1,-1,0]), null,3))
            .join(new Tween(this.sprite4, "y", 100, 3, Tween.getBezier([0,0.1,0.8,1,1,1,2,2,1,0.5,-0.1,0]), null,3))
            );


    }

    restart() {
        this.scene.restart();
    }

}