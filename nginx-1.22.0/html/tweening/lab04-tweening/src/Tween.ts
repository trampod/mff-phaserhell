import * as Phaser from "Phaser";


export default class Tween {


    public target;

    public property: string;

    public timeSecs: number;

    public duration: number;

    public startValue: number;

    public targetValue: number;

    public func: (t: number) => number;

    public on_end: (t: Tween) => void;

    public repeat : number;



    public constructor(target, property: string, targetValue: number, timeSecs: number, func: (t: number) => number, on_end: (t: Tween) => void = null, repeat : number = 1)
    {

        this.target = target;

        this.property = property;

        // so the sequences and groups work
        if(this.target && this.property)
            this.startValue = this.target[this.property];

        this.targetValue = targetValue;

        this.timeSecs = 0;

        this.duration = timeSecs;

        this.func = func;

        this.on_end = on_end;

        this.repeat = repeat;
    }

    public refreshStartValue()
    {
        if(this.target && this.property)
            this.startValue = this.target[this.property];
    }


    public update(time: number, deltaMillis: number)
    {     

        this.timeSecs += deltaMillis / 1000;

        this.target[this.property] = this.applyContext(this.func(this.convertT(this.timeSecs)));

    }


    protected applyContext(x: number) : number
    {

        return this.startValue * (1-x) + this.targetValue * x;

    }


    protected convertT(t: number) : number
    {

        return this.timeSecs/this.duration;

    }

    
    public isDone() : boolean
    {

        let result : boolean = this.timeSecs >= this.duration;



        if(result)
        {
            this.repeat--;
            if(this.repeat <= 0)
            {
                this.timeSecs = this.duration;
            }
            else
            {
                this.timeSecs -= this.duration;
            }
            this.update(0,0);
        }

        return this.timeSecs >= this.duration;
    }


    // -------------------------------------

    // FOLLOWS THE STASH OF EASING FUNCTIONS

    // -------------------------------------


    public static linear(t: number) : number {

        return t;

    }


    public static smoothStart2(t: number) : number {

        return t*t;

    }


    private static _smoothStartN(t: number, n: number) : number {

        let tmp = 1;

        for (let i = 0; i < n; i++) {

            tmp *= t;

        }

        return tmp;

    }


    public static getSmoothStartN(n: number) : (t: number) => number

    {

        //if(Number.isInteger(n))

        if(n == Math.floor(n))

            return (t:number) => this._smoothStartN(t,n);

        
        return (t:number) => this.linearBezie(this._smoothStartN(t,n),this._smoothStartN(t,n+1),n-Math.floor(n));

    }


    public static smoothStop2(t: number) : number {
        let d = 1 - t;
        
        let g = Tween.smoothStart2(d);

        return 1 - g;
    }


    private static _smoothStopN(t: number, n: number) : number {

        return (1-t)*this._smoothStartN(1-t,n);

    }


    public static getSmoothStopN(n: number) : (t: number) => number
    {

        if(n == Math.floor(n))

            return (t:number) => this._smoothStopN(t,n);

        
        return (t:number) => this.linearBezie(this._smoothStopN(t,n),this._smoothStopN(t,n+1),n-Math.floor(n));

    }


    public static smoothStep3(t: number) : number {
        return this.linearBezie(Tween.smoothStart2(t),Tween.smoothStop2(t),t);
    }


    public static hesitate(t:number) : number {

        return this.cubicBezie(0,1,0,1,t);

    }



    public static arch2(t:number) : number {

        return t*(1-t);

    }


    public static smoothStartArch3(t:number) : number {

        return t*t*(1-t);

    }


    public static smoothStopArch2(t:number) : number {

        return t*(1-t)*(1-t);

    }


    public static smoothStepArch2(t:number) : number {

        return t*t*(1-t)*(1-t);

    }


    public static bellCurve6(t:number):number{

        return this._smoothStartN(3,t) * this._smoothStopN(3,t);

    }





    private static bounceTop(x:number):number{

        return Math.abs(x);

    }


    private static bounceBottom(x:number):number{

        return 1 - Math.abs(1-x);

    }


    private static bounceTopBottom(x:number):number{

        return Tween.bounceTop(Tween.bounceBottom(x));

    }


    private static linearBezie(start:number, end:number, t: number) : number {

        return (1-t)*start + t*end;

    }


    private static quadraticBezie(start:number, mid:number, end:number, t: number) : number {

        return (1-t)*(1-t)*start + (1-t)*t*mid + t*t*end;

    }


    private static cubicBezie(start:number, mid1:number, mid2:number, end:number, t: number) : number {

        return (1-t)*(1-t)*(1-t)*start + (1-t)*(1-t)*t*mid1 + (1-t)*t*t*mid2 + t*t*t*end;

    }

    private static BezierN(points:number[],t:number) : number
    {
        if(points.length < 2)
            return 0;
        if(points.length == 2)
            return this.linearBezie(points[0],points[1],t);

        let i : number;
        let newPoints : number[] = [];

        for (i = 1; i < points.length; i++)
        {
            newPoints.push(this.linearBezie(points[i-1],points[i],t));
        }

        return this.BezierN(newPoints,t);
    }

    public static getBezier(points : number[]) : (t: number) => number
    {
        return (t:number) => this.BezierN(points, t);
    }

    public static funkyBezier(x: number) : number
    {
        return Tween.BezierN([0,1.5,0.2,2,1.3,0,-1,3,2,2,0.2,1,0,1],x);
    }

    //Following easing functions are from https://easings.net/

    public static easeInElastic(x:number) : number
    {
        let c4 = (2 * Math.PI) / 3;

        return x == 0
        ? 0
        : x == 1
            ? 1
            : -Math.pow(2, 10 * x - 10) * Math.sin((x * 10 - 10.75) * c4);
    }

    public static easeOutElastic(x: number) : number
    {
        let c4 = (2 * Math.PI) / 3;
        
        return x === 0
          ? 0
          : x === 1
            ? 1
            : Math.pow(2, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
        
    }

    public static easeInOutElastic(x: number) : number
    {
        let c5 = (2 * Math.PI) / 4.5;
        
        return x === 0
          ? 0
          : x === 1
            ? 1
            : x < 0.5
                ? -(Math.pow(2, 20 * x - 10) * Math.sin((20 * x - 11.125) * c5)) / 2
                : (Math.pow(2, -20 * x + 10) * Math.sin((20 * x - 11.125) * c5)) / 2 + 1;
    }


    public static easeInBounce(x: number): number
    {
        return 1 - Tween.easeOutBounce(1 - x);    
    }

    public static easeOutBounce(x: number): number
    {
        const n1 = 7.5625;
        const d1 = 2.75;
        
        if (x < 1 / d1) {
            return n1 * x * x;
        } else if (x < 2 / d1) {
            return n1 * (x -= 1.5 / d1) * x + 0.75;
        } else if (x < 2.5 / d1) {
            return n1 * (x -= 2.25 / d1) * x + 0.9375;
        } else {
            return n1 * (x -= 2.625 / d1) * x + 0.984375;
        }
    }

    public static easeInOutBounce(x: number): number
    {
        return x < 0.5
          ? (1 - Tween.easeOutBounce(1 - 2 * x)) / 2
          : (1 + Tween.easeOutBounce(2 * x - 1)) / 2;    
    }

}
