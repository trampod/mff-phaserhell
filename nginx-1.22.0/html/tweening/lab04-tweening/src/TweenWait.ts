import * as Phaser from "Phaser";
import Tween from "./Tween";


export default class TweenGroup extends Tween
{
    public constructor(duration : number, on_end: (t: Tween) => void = null)
    {
		super(null, null, null, duration, null, on_end);
    }

    public update(time: number, deltaMillis: number)
	{
        this.timeSecs += deltaMillis / 1000;
    }
    
    public isDone() : boolean
	{
        return this.timeSecs >= this.duration;
    }
}