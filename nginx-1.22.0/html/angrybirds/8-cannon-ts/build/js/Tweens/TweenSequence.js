define(["require", "exports", "./Tween", "./TweenGroup", "./TweenWait"], function (require, exports, Tween_1, TweenGroup_1, TweenWait_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /// This class should work similary to DOTween sequence, to be more familiar and useful
    class TweenSequence extends Tween_1.default {
        constructor(on_end = null) {
            super(null, null, null, 0, null, on_end);
            this.tweenList = [[0, new TweenGroup_1.default([new TweenWait_1.default(0)], null)]];
        }
        update(time, deltaMillis) {
            if (this.tweenList.length == 0)
                return;
            let oldTime = this.timeSecs;
            this.timeSecs += deltaMillis / 1000;
            let element = this.tweenList[0];
            while (element[0] < this.timeSecs) {
                if (element[0] < oldTime)
                    element[1].update(time, deltaMillis);
                else
                    element[1].update(element[0], this.timeSecs - element[0]);
                if (element[1].isDone()) {
                    this.tweenList.splice(0, 1);
                    if (element[1].on_end)
                        element[1].on_end(element[1]);
                    if (this.isDone())
                        return;
                    element = this.tweenList[0];
                    element[1].refreshStartValue();
                }
                else {
                    return;
                }
            }
        }
        refreshStartValue() {
            if (this.tweenList.length > 0)
                this.tweenList[0][1].refreshStartValue();
        }
        isDone() {
            return this.tweenList.length == 0;
        }
        setOnEnd(on_end = null) {
            this.on_end = on_end;
        }
        append(t) {
            this.tweenList.push([this.duration, new TweenGroup_1.default([t], null)]);
            this.duration += t.duration * t.repeat;
            return this;
        }
        join(t) {
            let index = this.tweenList.length - 1;
            this.tweenList[index][1].addTween(t);
            this.duration = this.tweenList[index][0] + this.tweenList[index][1].duration;
            return this;
        }
        break() {
            this.append(new TweenWait_1.default(0));
            return this;
        }
        static createSequence(tweens, on_end = null) {
            let seq = new TweenSequence(on_end);
            for (var tween of tweens) {
                seq.append(tween);
            }
            return seq;
        }
    }
    exports.default = TweenSequence;
});
//# sourceMappingURL=TweenSequence.js.map