define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Tween {
        constructor(target, property, targetValue, timeSecs, func, on_end = null, repeat = 1) {
            this.target = target;
            this.property = property;
            // so the sequences and groups work
            if (this.target && this.property)
                this.startValue = this.target[this.property];
            this.targetValue = targetValue;
            this.timeSecs = 0;
            this.duration = timeSecs;
            this.func = func;
            this.on_end = on_end;
            this.repeat = repeat;
        }
        refreshStartValue() {
            if (this.target && this.property)
                this.startValue = this.target[this.property];
        }
        update(time, deltaMillis) {
            // TODO: apply the tween correctly
            this.timeSecs += deltaMillis / 1000;
            this.target[this.property] = this.applyContext(this.func(this.convertT(this.timeSecs)));
        }
        applyContext(x) {
            return this.startValue * (1 - x) + this.targetValue * x;
        }
        convertT(t) {
            return this.timeSecs / this.duration;
        }
        isDone() {
            // TODO: after you adapt update(), check if this method still correctly
            //       reports your tween has finished
            let result = this.timeSecs >= this.duration;
            if (result) {
                this.repeat--;
                if (this.repeat <= 0) {
                    this.timeSecs = this.duration;
                }
                else {
                    this.timeSecs -= this.duration;
                }
                this.update(0, 0);
            }
            return this.timeSecs >= this.duration;
        }
        // -------------------------------------
        // FOLLOWS THE STASH OF EASING FUNCTIONS
        // -------------------------------------
        static linear(t) {
            return t;
        }
        static smoothStart2(t) {
            return t * t;
        }
        static _smoothStartN(t, n) {
            let tmp = 1;
            for (let i = 0; i < n; i++) {
                tmp *= t;
            }
            return tmp;
        }
        static getSmoothStartN(n) {
            //if(Number.isInteger(n))
            if (n == Math.floor(n))
                return (t) => this._smoothStartN(t, n);
            return (t) => this.linearBezie(this._smoothStartN(t, n), this._smoothStartN(t, n + 1), n - Math.floor(n));
        }
        static smoothStop2(t) {
            let d = 1 - t;
            let g = Tween.smoothStart2(d);
            return 1 - g;
        }
        static _smoothStopN(t, n) {
            return (1 - t) * this._smoothStartN(1 - t, n);
        }
        static getSmoothStopN(n) {
            //if(Number.isInteger(n))
            if (n == Math.floor(n))
                return (t) => this._smoothStopN(t, n);
            return (t) => this.linearBezie(this._smoothStopN(t, n), this._smoothStopN(t, n + 1), n - Math.floor(n));
        }
        static smoothStep3(t) {
            return this.linearBezie(Tween.smoothStart2(t), Tween.smoothStop2(t), t);
        }
        static hesitate(t) {
            return this.cubicBezie(0, 1, 0, 1, t);
        }
        static arch2(t) {
            return t * (1 - t);
        }
        static smoothStartArch3(t) {
            return t * t * (1 - t);
        }
        static smoothStopArch2(t) {
            return t * (1 - t) * (1 - t);
        }
        static smoothStepArch2(t) {
            return t * t * (1 - t) * (1 - t);
        }
        static bellCurve6(t) {
            return this._smoothStartN(3, t) * this._smoothStopN(3, t);
        }
        static bounceTop(x) {
            return Math.abs(x);
        }
        static bounceBottom(x) {
            return 1 - Math.abs(1 - x);
        }
        static bounceTopBottom(x) {
            return Tween.bounceTop(Tween.bounceBottom(x));
        }
        static linearBezie(start, end, t) {
            return (1 - t) * start + t * end;
        }
        static quadraticBezie(start, mid, end, t) {
            return (1 - t) * (1 - t) * start + (1 - t) * t * mid + t * t * end;
        }
        static cubicBezie(start, mid1, mid2, end, t) {
            return (1 - t) * (1 - t) * (1 - t) * start + (1 - t) * (1 - t) * t * mid1 + (1 - t) * t * t * mid2 + t * t * t * end;
        }
        static BezierN(points, t) {
            if (points.length < 2)
                return 0;
            if (points.length == 2)
                return this.linearBezie(points[0], points[1], t);
            let i;
            let newPoints = [];
            for (i = 1; i < points.length; i++) {
                newPoints.push(this.linearBezie(points[i - 1], points[i], t));
            }
            return this.BezierN(newPoints, t);
        }
        static getBezier(points) {
            return (t) => this.BezierN(points, t);
        }
        static funkyBezier(x) {
            //return Tween.cubicBezie(0,1.5,0.2,1,x);
            return Tween.BezierN([0, 1.5, 0.2, 2, 1.3, 0, -1, 3, 2, 2, 0.2, 1, 0, 1], x);
        }
        // TODO: add more easing functions here
        //Following easing functions are from https://easings.net/
        static easeInElastic(x) {
            let c4 = (2 * Math.PI) / 3;
            return x == 0
                ? 0
                : x == 1
                    ? 1
                    : -Math.pow(2, 10 * x - 10) * Math.sin((x * 10 - 10.75) * c4);
        }
        static easeOutElastic(x) {
            let c4 = (2 * Math.PI) / 3;
            return x === 0
                ? 0
                : x === 1
                    ? 1
                    : Math.pow(2, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
        }
        static easeInOutElastic(x) {
            let c5 = (2 * Math.PI) / 4.5;
            return x === 0
                ? 0
                : x === 1
                    ? 1
                    : x < 0.5
                        ? -(Math.pow(2, 20 * x - 10) * Math.sin((20 * x - 11.125) * c5)) / 2
                        : (Math.pow(2, -20 * x + 10) * Math.sin((20 * x - 11.125) * c5)) / 2 + 1;
        }
        static easeInBounce(x) {
            return 1 - Tween.easeOutBounce(1 - x);
        }
        static easeOutBounce(x) {
            const n1 = 7.5625;
            const d1 = 2.75;
            if (x < 1 / d1) {
                return n1 * x * x;
            }
            else if (x < 2 / d1) {
                return n1 * (x -= 1.5 / d1) * x + 0.75;
            }
            else if (x < 2.5 / d1) {
                return n1 * (x -= 2.25 / d1) * x + 0.9375;
            }
            else {
                return n1 * (x -= 2.625 / d1) * x + 0.984375;
            }
        }
        static easeInOutBounce(x) {
            return x < 0.5
                ? (1 - Tween.easeOutBounce(1 - 2 * x)) / 2
                : (1 + Tween.easeOutBounce(2 * x - 1)) / 2;
        }
    }
    exports.default = Tween;
});
//# sourceMappingURL=Tween.js.map