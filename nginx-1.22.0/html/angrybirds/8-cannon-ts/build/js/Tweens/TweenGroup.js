define(["require", "exports", "./Tween"], function (require, exports, Tween_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class TweenGroup extends Tween_1.default {
        constructor(tweens, on_end = null) {
            super(null, null, null, 0, null, on_end);
            if (tweens != null)
                this.tweenList = tweens;
            else
                this.tweenList = [];
            for (var tween of this.tweenList) {
                if (tween.duration * tween.repeat > this.duration) {
                    this.duration = tween.duration * tween.repeat;
                }
            }
        }
        refreshStartValue() {
            for (var tween of this.tweenList) {
                tween.refreshStartValue();
            }
        }
        update(time, deltaMillis) {
            let i = 0;
            while (i < this.tweenList.length) {
                let element = this.tweenList[i];
                element.update(time, deltaMillis);
                if (element.isDone()) {
                    this.tweenList.splice(i, 1);
                    if (element.on_end) {
                        element.on_end(element);
                    }
                }
                else {
                    i += 1;
                }
            }
        }
        isDone() {
            return this.tweenList.length == 0;
        }
        addTween(t) {
            this.tweenList.push(t);
        }
    }
    exports.default = TweenGroup;
});
//# sourceMappingURL=TweenGroup.js.map