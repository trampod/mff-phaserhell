define(["require", "exports", "./Tween"], function (require, exports, Tween_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class TweenGroup extends Tween_1.default {
        constructor(duration, on_end = null) {
            super(null, null, null, duration, null, on_end);
        }
        update(time, deltaMillis) {
            this.timeSecs += deltaMillis / 1000;
        }
        isDone() {
            return this.timeSecs >= this.duration;
        }
    }
    exports.default = TweenGroup;
});
//# sourceMappingURL=TweenWait.js.map