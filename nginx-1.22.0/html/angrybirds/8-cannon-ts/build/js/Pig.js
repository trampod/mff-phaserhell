define(["require", "exports", "./Tweens/Tween", "./Tweens/TweenSequence", "./Tweens/TweenWait"], function (require, exports, Tween_1, TweenSequence_1, TweenWait_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Pig {
        constructor(play, sprite, states, health, position, scale, massModifier, resistance) {
            this.play = play;
            this.gameObject = this.play.matter.add.image(position[0], position[1], sprite, states[0][0]);
            this.gameObject.setTint(states[0][1]);
            this.gameObject.setScale(scale[0], scale[1]);
            this.gameObject.setFlipX(true);
            this.gameObject["pig"] = this;
            this.states = states;
            this.sprite = sprite;
            this.health = health;
            this.resistance = resistance;
            this.gameObject.body.mass *= massModifier;
        }
        applyHit(momentum) {
            if (momentum < this.resistance)
                return;
            this.health--;
            if (this.health <= 0) {
                let image = this.play.add.image(this.gameObject.x, this.gameObject.y, this.sprite, this.states[1][0]);
                image.setScale(this.gameObject.scaleX, this.gameObject.scaleY);
                image.setRotation(this.gameObject.rotation);
                image.alpha = 0.7;
                image.setTint(this.states[1][1]);
                image.setFlipX(true);
                let seq = new TweenSequence_1.default(() => image.destroy());
                seq.join(new Tween_1.default(image, "alpha", 0, 0.3, Tween_1.default.smoothStart2));
                seq.join(new Tween_1.default(image, "scaleX", image.scaleX * 1.1, 0.3, Tween_1.default.smoothStop2));
                seq.join(new Tween_1.default(image, "scaleY", image.scaleY * 1.1, 0.3, Tween_1.default.smoothStop2));
                this.play.tw.addTween(seq);
                this.gameObject.destroy();
            }
            else {
                this.gameObject.setFrame(this.states[1][0]);
                this.gameObject.setTint(this.states[1][1]);
                this.play.tw.addTween(new TweenWait_1.default(0.3, () => {
                    this.gameObject.setFrame(this.states[0][0]);
                    this.gameObject.setTint(this.states[0][1]);
                }));
            }
        }
    }
    exports.default = Pig;
});
//# sourceMappingURL=Pig.js.map