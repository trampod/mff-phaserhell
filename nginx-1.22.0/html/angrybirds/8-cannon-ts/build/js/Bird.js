define(["require", "exports", "./Tweens/Tween", "./Tweens/TweenSequence"], function (require, exports, Tween_1, TweenSequence_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Bird {
        constructor(play, gameObject, timeout) {
            this.play = play;
            this.gameObject = gameObject;
            this.gameObject["bird"] = this;
            this._counter = Infinity;
            this.timeout = timeout;
        }
        applyHit(momentum) {
            this._counter = this.timeout;
        }
        isDead() {
            return this._counter <= 0;
        }
        animateDeath() {
            let image = this.play.add.sprite(this.gameObject.x, this.gameObject.y, "chick");
            image.setScale(this.gameObject.scaleX, this.gameObject.scaleY);
            image.setRotation(this.gameObject.rotation);
            image.play("fly");
            let seq = new TweenSequence_1.default(() => image.destroy());
            seq.join(new Tween_1.default(image, "alpha", 0, 0.3, Tween_1.default.smoothStart2));
            this.play.tw.addTween(seq);
        }
    }
    exports.default = Bird;
});
//# sourceMappingURL=Bird.js.map