define(["require", "exports", "Phaser", "./Bird", "./Block", "./Pig", "./Tweens/Tweens"], function (require, exports, Phaser, Bird_1, Block_1, Pig_1, Tweens_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Play extends Phaser.Scene {
        constructor() {
            super("Play");
            this.blocks = [];
            this.pigs = [];
            this.birdStorage = [];
            this.birdCount = 40;
            this.shotBirds = [];
            this.holding = false;
            this.holdTime = 0;
            this.maxHold = 2;
            this.cooldown = 2;
            this._counter = 0;
            this.selectedMap = 1;
        }
        update(time, delta) {
            this.tw.update(time, delta);
            if (this.holding) {
                this.holdTime += delta / 1000;
                if (this.holdTime > this.maxHold)
                    this.holdTime = this.maxHold;
                if (this.pointer != null) {
                    Phaser.Geom.Line.SetToAngle(this.line, this.cannon.x, this.cannon.y - 50, this.angle, 64 + 32 * this.holdTime);
                }
            }
            else {
                Phaser.Geom.Line.SetToAngle(this.line, this.cannon.x, this.cannon.y - 50, this.angle, 64);
            }
            for (let i = this.shotBirds.length - 1; i >= 0; i--) {
                this.shotBirds[i]._counter -= delta / 1000;
                if (this.shotBirds[i].isDead()) {
                    this.shotBirds[i].animateDeath();
                    this.shotBirds[i].gameObject.destroy();
                    this.shotBirds.splice(i, 1);
                }
            }
            this._counter -= delta / 1000;
            if (this._counter < 0) {
                this._counter = 0;
            }
            this.gfx.clear();
            if (this._counter > 0) {
                Phaser.Geom.Line.SetToAngle(this.cooldownLine, this.cannon.x - 42, this.cannon.y + 45, -Math.PI / 2, 125 * (this._counter / this.cooldown));
                this.gfx.strokeLineShape(this.cooldownLine);
            }
            else {
                this.gfx.strokeLineShape(this.line);
            }
        }
        setupGame(map) {
            this.selectedMap = map;
            this.resetGame();
        }
        resetGame() {
            for (let i = 0; i < this.pigs.length; i++) {
                this.pigs[i].gameObject.destroy();
            }
            this.pigs = [];
            for (let i = 0; i < this.blocks.length; i++) {
                this.blocks[i].gameObject.destroy();
            }
            this.blocks = [];
            for (let i = 0; i < this.shotBirds.length; i++) {
                this.shotBirds[i].gameObject.destroy();
            }
            this.shotBirds = [];
            for (let i = 0; i < this.birdStorage.length; i++) {
                this.birdStorage[i].destroy();
            }
            this.birdStorage = [];
            this._counter = this.cooldown;
            switch (this.selectedMap) {
                case 1:
                    this.map1();
                    break;
                case 2:
                    this.map2();
                    break;
                case 3:
                    this.map3();
                    break;
                default:
                    this.testMap();
                    break;
            }
            for (let i = 0; i < this.birdCount; i++) {
                // @ts-ignore
                this.birdStorage.push(this.matter.add.sprite(this.cannon.x - 70, this.cannon.y - 50 - 500 * i, 'chick').setBounce(0.7).setFriction(0.3).setScale(2));
                this.birdStorage[i].body.mass = 5;
            }
        }
        testMap() {
            this.birdCount = 40;
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 3, [1000, 450], [3, 3], 1, 8));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 3, [900, 450], [3, 3], 1, 8));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 3, [1200, 450], [3, 3], 1, 8));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1100, 300], [0.5, 1], 1, 30));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1300, 300], [0.5, 1], 1, 30));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xffffff, [1200, 100], [1, 0.5], 1, 30));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal055.png"], 0xffffff, [500, 300], [0.5, 1], 20, 0.1));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal055.png"], 0xffffff, [600, 200], [0.5, 1], 20, 0.1));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal055.png"], 0xffffff, [700, 100], [0.5, 1], 20, 0.1));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal055.png"], 0xffffff, [800, 0], [0.5, 1], 20, 0.1));
            // for (let i = 0; i < 20; i++) {
            //     this.blocks.push(new Block(this,"metal",["elementMetal020.png","elementMetal025.png","elementMetal055.png"],[300+50*i,400 - 10*i],[0.5,1],20,30));
            // }
        }
        map3() {
            this.birdCount = 20;
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1220, 457], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1020, 457], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [800, 457], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [600, 457], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [1130, 457 - 55 - 20], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [910, 457 - 55 - 20], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [690, 457 - 55 - 20], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [1220, 457 - 110 - 35], [0.5, 0.5], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [1040, 457 - 110 - 35], [0.5, 0.5], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [780, 457 - 110 - 35], [0.5, 0.5], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [600, 457 - 110 - 35], [0.5, 0.5], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [1130, 457 - 55 - 20 - 145], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [690, 457 - 55 - 20 - 145], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1220, 457 - 110 - 35 - 145], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1040, 457 - 110 - 35 - 145], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [780, 457 - 110 - 35 - 145], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [600, 457 - 110 - 35 - 145], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [1130, 457 - 55 - 20 - 145 - 145], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [690, 457 - 55 - 20 - 145 - 145], [1, 0.5], 1, 20));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [1130, 457 - 55 - 20 - 20 - 145], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [690, 457 - 55 - 20 - 20 - 145], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [910, 457 - 55 - 20 - 20 - 20], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [850, 485], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 3, [910, 477], [4, 4], 1, 8));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [970, 485], [3, 3], 1, 6));
        }
        map2() {
            this.birdCount = 10;
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1220, 457], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1040, 457], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [780, 457], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [600, 457], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [1130, 457 - 55 - 20], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [690, 457 - 55 - 20], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [1220, 457 - 110 - 35], [0.5, 0.5], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [1040, 457 - 110 - 35], [0.5, 0.5], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [780, 457 - 110 - 35], [0.5, 0.5], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [600, 457 - 110 - 35], [0.5, 0.5], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [1130, 457 - 55 - 20 - 145], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [690, 457 - 55 - 20 - 145], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1220, 457 - 110 - 35 - 145], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [1040, 457 - 110 - 35 - 145], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [780, 457 - 110 - 35 - 145], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png", "elementMetal055.png"], 0xffffff, [600, 457 - 110 - 35 - 145], [0.5, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [1130, 457 - 55 - 20 - 145 - 145], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [690, 457 - 55 - 20 - 145 - 145], [1, 0.5], 1, 20));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [1130, 457 - 55 - 20 - 20 - 145], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [690, 457 - 55 - 20 - 20 - 145], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [1130, 457 - 55 - 20 - 20 - 20], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [690, 457 - 55 - 20 - 20 - 20], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [1130, 485], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [690, 485], [3, 3], 1, 6));
        }
        map1() {
            this.birdCount = 5;
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [1140, 402], [0.5, 1], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [920, 402], [0.5, 1], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal020.png", "elementMetal025.png", "elementMetal055.png"], 0xbbddff, [700, 402], [0.5, 1], 1, 5));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [1030, 402 - 110 - 20], [1, 0.5], 1, 20));
            this.blocks.push(new Block_1.default(this, "metal", ["elementMetal013.png", "elementMetal016.png", "elementMetal051.png", "elementMetal051.png"], 0xbbbbbb, [810, 402 - 110 - 20], [1, 0.5], 1, 20));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [1030, 485], [3, 3], 1, 6));
            this.pigs.push(new Pig_1.default(this, "chick", [[0, 0x009900], [3, 0xff0000]], 2, [810, 485], [3, 3], 1, 6));
        }
        preload() {
            this.load.bitmapFont('desyrel', 'assets/fonts/desyrel.png', 'assets/fonts/desyrel.xml');
            this.load.bitmapFont('stack', 'assets/fonts/shortStack.png', 'assets/fonts/shortStack.xml');
        }
        create() {
            this.tw = new Tweens_1.default(this.game);
            console.log("Play.create()");
            this.anims.create({ key: 'fly', frames: this.anims.generateFrameNumbers('chick', { start: 0, end: 3 }), frameRate: 5, repeat: -1 });
            this.add.image(320, 256, 'backdrop').setScale(2);
            this.add.image(320 + 385, 256, 'backdrop').setScale(2);
            this.add.image(320 + 385 * 2, 256, 'backdrop').setScale(2);
            this.add.image(320 + 385 * 3, 256, 'backdrop').setScale(2);
            this.cannonHead = this.add.image(130, 416, 'cannon_head').setDepth(1);
            this.cannon = this.add.image(130, 464, 'cannon_body').setDepth(1);
            // @ts-ignore
            //this.chick = this.matter.add.sprite(this.cannon.x, this.cannon.y - 50, 'chick').setBounce(0.7).setFriction(0.3).setScale(2);
            this.gfx = this.add.graphics().setDefaultStyles({ lineStyle: { width: 10, color: 0xff8800, alpha: 0.5 } });
            this.line = new Phaser.Geom.Line();
            this.angle = 0;
            this.cooldownLine = new Phaser.Geom.Line();
            this.setupGame(1);
            this.input.keyboard.on('keydown-R', this.resetGame, this);
            this.input.keyboard.on('keydown-ONE', () => this.setupGame(1), this);
            this.input.keyboard.on('keydown-TWO', () => this.setupGame(2), this);
            this.input.keyboard.on('keydown-THREE', () => this.setupGame(3), this);
            this.input.keyboard.on('keydown-T', () => this.setupGame(-1), this);
            this.add.bitmapText(130, 80, 'desyrel', 'AngryChicks', 64).setAngle(-5);
            // this.add.bitmapText(400, 400, 'stack', '1,2,3 to select maps', 32); // doesn't have numbers :|
            this.add.text(220, 140, '1,2,3 to select maps', { font: "32px Arial" }).setAngle(5)
                .setTintFill(0x006600, 0x002200, 0xbb0000, 0x006600)
                .setTint(0x008800, 0x004400, 0xff0000, 0x008800)
                .setStroke("black", 3);
            this.input.on('pointermove', function (pointer) {
                if (pointer.x > this.cannon.x) {
                    this.pointer = pointer;
                    this.angle = Phaser.Math.Angle.BetweenPoints(this.cannonHead, this.pointer);
                    this.cannonHead.rotation = this.angle;
                }
            }, this);
            this.input.on('pointerdown', function () {
                if (this._counter <= 0 && this.birdStorage.length > 0) {
                    this.holding = true;
                    this.holdTime = 0;
                    this.gfx.defaultStrokeColor = 0xff0000;
                }
            }, this);
            this.input.on('pointerup', function () {
                if (this.holding && this.birdStorage.length > 0) {
                    this.holding = false;
                    this.gfx.defaultStrokeColor = 0xff8800;
                    this._counter = this.cooldown;
                    this.chick = this.birdStorage[0];
                    this.birdStorage.shift();
                    this.shotBirds.push(new Bird_1.default(this, this.chick, 3));
                    this.chick.play("fly");
                    this.chick.setStatic(false);
                    this.chick.x = this.cannon.x;
                    this.chick.y = this.cannon.y - 50;
                    this.chick.rotation = 0;
                    this.chick.setAngularVelocity(0);
                    this.chick.setVelocity((this.line.x2 - this.line.x1) / 5, (this.line.y2 - this.line.y1) / 5);
                }
            }, this);
            //@ts-ignore
            this.ground = this.matter.add.image(600, 863, 'metal', 'elementMetal013.png');
            this.ground.setStatic(true);
            this.ground.setScale(10);
            this.ground.body.gameObject.setTint(0xaa4400);
            this.matter.world.on('collisionstart', function (event, bodyA, bodyB) {
                let relativeSpeedMagnitude = Math.sqrt(Math.pow(bodyA.velocity.x - bodyB.velocity.x, 2) + Math.pow(bodyA.velocity.y - bodyB.velocity.y, 2));
                if (Number.isNaN(relativeSpeedMagnitude))
                    relativeSpeedMagnitude = 0;
                let momentumA = bodyA.mass * relativeSpeedMagnitude;
                let momentumB = bodyB.mass * relativeSpeedMagnitude;
                this.processCollision(bodyA.gameObject, momentumB == Infinity ? momentumA : momentumB);
                this.processCollision(bodyB.gameObject, momentumA == Infinity ? momentumB : momentumA);
            }, this);
        }
        hitMetal(metal, chick) {
            console.log("I won");
            metal.body.setAllowGravity(true);
        }
        processCollision(body, momentum) {
            if (body["block"] != null)
                body["block"].applyHit(momentum);
            if (body["pig"] != null)
                body["pig"].applyHit(momentum);
            if (body["bird"] != null)
                body["bird"].applyHit(momentum);
        }
    }
    exports.default = Play;
});
//# sourceMappingURL=Play.js.map