define(["require", "exports", "./Tweens/Tween", "./Tweens/TweenSequence"], function (require, exports, Tween_1, TweenSequence_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Block {
        constructor(play, sprite, states, tint, position, scale, massModifier, resistance) {
            this.state = 0;
            this.play = play;
            this.gameObject = this.play.matter.add.image(position[0], position[1], sprite, states[this.state]);
            this.gameObject.setScale(scale[0], scale[1]);
            this.gameObject["block"] = this;
            this.states = states;
            this.sprite = sprite;
            this.resistance = resistance;
            this.gameObject.body.mass *= massModifier;
            this.gameObject.setTint(tint);
        }
        applyHit(momentum) {
            if (momentum < this.resistance)
                return;
            this.state++;
            if (this.state < this.states.length - 1) {
                this.gameObject.setFrame(this.states[this.state]);
            }
            else {
                this.gameObject.setTint(0x222222);
                let image = this.play.add.image(this.gameObject.x, this.gameObject.y, this.sprite, this.states[this.states.length - 1]);
                image.setScale(this.gameObject.scaleX, this.gameObject.scaleY);
                image.setRotation(this.gameObject.rotation);
                image.alpha = 0.7;
                image.setTint(this.gameObject.tint);
                let seq = new TweenSequence_1.default(() => image.destroy());
                seq.join(new Tween_1.default(image, "alpha", 0, 0.3, Tween_1.default.smoothStart2));
                seq.join(new Tween_1.default(image, "scaleX", image.scaleX * 1.1, 0.3, Tween_1.default.smoothStop2));
                seq.join(new Tween_1.default(image, "scaleY", image.scaleY * 1.1, 0.3, Tween_1.default.smoothStop2));
                this.play.tw.addTween(seq);
                this.gameObject.destroy();
            }
        }
    }
    exports.default = Block;
});
//# sourceMappingURL=Block.js.map