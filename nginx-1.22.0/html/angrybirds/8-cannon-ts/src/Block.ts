import * as Phaser from "Phaser";
import Play from "./Play";
import Tween from "./Tweens/Tween";
import TweenGroup from "./Tweens/TweenGroup";
import Tweens from "./Tweens/Tweens";
import TweenSequence from "./Tweens/TweenSequence";
import TweenWait from "./Tweens/TweenWait";


export default class Block
{
	gameObject : Phaser.Physics.Matter.Image;

	play : Play;
	
	sprite : string;
	state : integer = 0;
	states : string[];
	resistance : number;

    public constructor(play : Play, sprite : string, states : string[], tint : number, position : [integer, integer], scale : [integer, integer], massModifier: number, resistance : number)
    {
		this.play = play;
		this.gameObject = this.play.matter.add.image(position[0],position[1],sprite,states[this.state]);
		this.gameObject.setScale(scale[0],scale[1]);
		this.gameObject["block"] = this;
		this.states = states;
		this.sprite = sprite;
		this.resistance = resistance;
		this.gameObject.body.mass *= massModifier;
		this.gameObject.setTint(tint);
    }

	applyHit(momentum : number)
	{
		if (momentum < this.resistance)
			return;

		this.state ++;
		if(this.state < this.states.length-1)
		{
			this.gameObject.setFrame(this.states[this.state]);
		}
		else
		{
			this.gameObject.setTint(0x222222);

			let image : Phaser.GameObjects.Image = this.play.add.image(this.gameObject.x,this.gameObject.y,this.sprite,this.states[this.states.length-1]);
			image.setScale(this.gameObject.scaleX, this.gameObject.scaleY);
			image.setRotation(this.gameObject.rotation);
			image.alpha = 0.7;
			image.setTint(this.gameObject.tint);

			let seq : TweenSequence = new TweenSequence(()=> image.destroy());

			seq.join(new Tween(image, "alpha", 0, 0.3, Tween.smoothStart2));
			seq.join(new Tween(image, "scaleX", image.scaleX*1.1, 0.3, Tween.smoothStop2));
			seq.join(new Tween(image, "scaleY", image.scaleY*1.1, 0.3, Tween.smoothStop2));

			this.play.tw.addTween(seq);

			this.gameObject.destroy();
		}
	}
}