import * as Phaser from "Phaser";
import Play from "./Play";
import Tween from "./Tweens/Tween";
import TweenGroup from "./Tweens/TweenGroup";
import Tweens from "./Tweens/Tweens";
import TweenSequence from "./Tweens/TweenSequence";
import TweenWait from "./Tweens/TweenWait";


export default class Pig
{
	gameObject : Phaser.Physics.Matter.Image;

	play : Play;
	
	health : integer;
	states : [[number,number],[number,number]];//[frame,tint]
	sprite : string;
	resistance : number;

    public constructor(play : Play, sprite : string, states : [[number,number],[number,number]], health : integer, position : [integer, integer], scale : [integer, integer], massModifier: number, resistance : number)
    {
		this.play = play;
		this.gameObject = this.play.matter.add.image(position[0],position[1],sprite,states[0][0]);
		this.gameObject.setTint(states[0][1]);
		this.gameObject.setScale(scale[0],scale[1]);
		this.gameObject.setFlipX(true);
		this.gameObject["pig"] = this;
		this.states = states;
		this.sprite = sprite;
		this.health = health;
		this.resistance = resistance;
		this.gameObject.body.mass *= massModifier;
    }

	applyHit(momentum : number)
	{

		if (momentum < this.resistance)
			return;
		this.health--;


		if(this.health <= 0)
		{
			let image : Phaser.GameObjects.Image = this.play.add.image(this.gameObject.x,this.gameObject.y,this.sprite,this.states[1][0]);
			image.setScale(this.gameObject.scaleX, this.gameObject.scaleY);
			image.setRotation(this.gameObject.rotation);
			image.alpha = 0.7;
			image.setTint(this.states[1][1]);
			image.setFlipX(true);

			let seq : TweenSequence = new TweenSequence(()=> image.destroy());

			seq.join(new Tween(image, "alpha", 0, 0.3, Tween.smoothStart2));
			seq.join(new Tween(image, "scaleX", image.scaleX*1.1, 0.3, Tween.smoothStop2));
			seq.join(new Tween(image, "scaleY", image.scaleY*1.1, 0.3, Tween.smoothStop2));

			this.play.tw.addTween(seq);
			

			this.gameObject.destroy();
		}
		else
		{
			this.gameObject.setFrame(this.states[1][0]);
			this.gameObject.setTint(this.states[1][1]);
	
			this.play.tw.addTween(new TweenWait(0.3,()=>{
				this.gameObject.setFrame(this.states[0][0]);
				this.gameObject.setTint(this.states[0][1]);
			}));
		}
	}
}