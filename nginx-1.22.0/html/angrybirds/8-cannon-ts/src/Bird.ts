import * as Phaser from "Phaser";
import Play from "./Play";
import Tween from "./Tweens/Tween";
import TweenGroup from "./Tweens/TweenGroup";
import Tweens from "./Tweens/Tweens";
import TweenSequence from "./Tweens/TweenSequence";
import TweenWait from "./Tweens/TweenWait";


export default class Bird
{
	gameObject : Phaser.Physics.Matter.Sprite;

	play : Play;
	
	timeout : number;

	_counter : number;

    public constructor(play : Play, gameObject : Phaser.Physics.Matter.Sprite, timeout : number)
    {
		this.play = play;
		this.gameObject = gameObject;
		this.gameObject["bird"] = this;
		this._counter = Infinity;
		this.timeout = timeout;
    }

	applyHit(momentum : number)
	{
		this._counter = this.timeout;
	}

	isDead()
	{
		return this._counter <= 0;
	}

    animateDeath()
	{
		let image : Phaser.GameObjects.Sprite = this.play.add.sprite(this.gameObject.x,this.gameObject.y,"chick");
		image.setScale(this.gameObject.scaleX, this.gameObject.scaleY);
		image.setRotation(this.gameObject.rotation);
		image.play("fly");

		let seq : TweenSequence = new TweenSequence(()=> image.destroy());

		seq.join(new Tween(image, "alpha", 0, 0.3, Tween.smoothStart2));

		this.play.tw.addTween(seq);
    }
}